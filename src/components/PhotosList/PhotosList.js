import React, {Component} from 'react';
import {FlatList, ActivityIndicator, View} from 'react-native';
import {endpoints as CE} from "../../constants";
import styles from "./PhotosList.style";
import PhotoListItem from "../PhotoListItem/PhotoListItem";
import connect from "react-redux/es/connect/connect";

class PhotosList extends Component {
    constructor() {
        super();
    }
    _listItem(item) {
        return <PhotoListItem
                    id={item.item.id}
                    author={item.item.user.name}
                    path={item.item.urls.thumb} />
    }

    render() {
        const { endpoints } = this.props;

        const data = endpoints[CE.PHOTOS_LIST];
        const list = data.loaded
            ? Object.keys(data.data).map(key => data.data[key])
            : [];

        if (data.loading) {
            return (
                <View style={styles.container}>
                    <ActivityIndicator  size='small' color='#555555' />
                </View>
            )
        } else if (data.loaded && !data.error) {
            return (
                <FlatList
                    data={list}
                    renderItem={this._listItem} />
            )
        } else {
            return (
                <View />
            );
        }
    }
}


const mapProps = (state) => state;
export default connect(mapProps)(PhotosList);