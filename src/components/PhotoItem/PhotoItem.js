import React, {Component} from 'react';
import {Image, View} from 'react-native';
import styles from "./PhotoItem.style";
import connect from "react-redux/es/connect/connect";
import {endpoints as CE} from "../../constants";

class PhotoItem extends Component {
    render() {
        const { endpoints } = this.props;
        const data = endpoints[CE.PHOTOS_LIST].data;
        const selection = this.props.app.selection;

        if (data && selection) {
            const key = Object.keys(data).find(key => {
                return data[key].id === selection
            });

            if (key >= 0) {
                return (
                    <Image source={{uri: data[key].urls.full}} style={styles.holder} />
                );
            } else {
                return (
                    <View />
                );
            }
        } else {
            return (
                <View />
            );
        }
    }
}

const mapProps = (state) => state;
export default connect(mapProps)(PhotoItem);