import {StyleSheet} from "react-native";

export default StyleSheet.create({
    holder: {
        width: '100%',
        height: '100%',
    },
});
