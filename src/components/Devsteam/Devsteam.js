import React, {Component} from 'react';
import {endpoints as CE, screens as CS} from "../../constants";
import {createAppContainer, createStackNavigator} from "react-navigation";
import PhotosList from "../PhotosList/PhotosList";
import PhotoItem from "../PhotoItem/PhotoItem";
import {loadEnpoint, setScreen, setToken} from "../../modules/actions";
import connect from "react-redux/es/connect/connect";
import PropTypes from "prop-types";
import {NavigationActions} from 'react-navigation';

const {LIST_SCREEN, ITEM_SCREEN} = CS;

const DevsteamNavigation = createStackNavigator({
    [LIST_SCREEN]: {
        screen: PhotosList,
        navigationOptions: () => ({
            title: 'Unsplash: Photos list',
        }),
    },
    [ITEM_SCREEN]: {
        screen: PhotoItem,
        navigationOptions: () => ({
            title: 'Unsplash: Photo',
        }),
    },
});


const AppContainer = createAppContainer(DevsteamNavigation);

class Devsteam extends Component {
    constructor() {
        super();
        this.navigationStateHandler = this._navigationStateHandler.bind(this);
    }
    _navigationStateHandler(prevState, nextState, action) {
        const current = getCurrentRouteName(nextState);
        if (this.props.app.screen !== current) {
            this.props.setScreen(current);
        }
    }
    componentDidMount() {
        this.props.setToken(this.props.token);
        this.props.setScreen(LIST_SCREEN);
        this.props.load(CE.PHOTOS_LIST);
    }
    componentDidUpdate() {
        if (this.props.app.screen !== getCurrentRouteName (this.navigator.state.nav)) {
            const navAction = NavigationActions.navigate({
                routeName: this.props.app.screen,
            });
            this.navigator.dispatch(navAction);
        }
    }

    render() {
        return <AppContainer
                    ref={nav => this.navigator = nav}
                    onNavigationStateChange={this.navigationStateHandler} />;
    }
}

function getCurrentRouteName(navState) {
    if (!navState) {
        return null;
    }
    const route = navState.routes[navState.index];
    return route.routeName;
}


Devsteam.propTypes = {
    token: PropTypes.string,
};
Devsteam.defaultProps = {
    token: 'cf49c08b444ff4cb9e4d126b7e9f7513ba1ee58de7906e4360afc1a33d1bf4c0',
};


const mapProps = (state) => state;
const mapDispatch = {
    load: loadEnpoint,
    setScreen: setScreen,
    setToken: setToken,
};

export default connect(mapProps, mapDispatch)(Devsteam);