import {StyleSheet} from "react-native";

export default StyleSheet.create({
    holder: {
        width: '100%',
        height: 200,
    },
    title: {
        fontSize: 14,
        fontFamily: 'Verdana',
        fontWeight: 'bold',
        color: '#FFFFFF',
        textAlign: 'right',
        marginTop: 5,
        marginRight: 15,
        shadowColor: '#000000',
        shadowOffset: { width: 0, height: 0 },
        shadowOpacity: 1,
        shadowRadius: 2,
    },
});
