import React, {Component} from 'react';
import {Text, ImageBackground, TouchableOpacity} from 'react-native';
import PropTypes from "prop-types";
import styles from "./PhotoListItem.style";
import {setScreen, selectPhoto} from "../../modules/actions";
import connect from "react-redux/es/connect/connect";
import {screens as CS} from "../../constants";

class PhotoListItem extends Component {
    constructor() {
        super();
        this.clickHandler = this._clickHandler.bind(this);
    }
    _clickHandler() {
        this.props.setScreen(CS.ITEM_SCREEN);
        this.props.selectPhoto(this.props.id);
    }
    render() {
        return (
            <TouchableOpacity onPress={this.clickHandler} activeOpacity={.9}>
                <ImageBackground source={{uri: this.props.path}} style={styles.holder}>
                    <Text style={styles.title}>{this.props.author}</Text>
                </ImageBackground>
            </TouchableOpacity>
        );
    }
}

PhotoListItem.propTypes = {
    id: PropTypes.string,
    path: PropTypes.string,
    author: PropTypes.string,
};
PhotoListItem.defaultProps = {
    id: '',
    path: '',
    author: '',
};

const mapProps = (state) => state;
const mapDispatch = {
    setScreen: setScreen,
    selectPhoto: selectPhoto,
};

export default connect(mapProps, mapDispatch)(PhotoListItem);
