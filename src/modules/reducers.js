import { handleActions } from 'redux-actions';
import { combineReducers } from 'redux';

import { actions as C } from '../constants';
import store from './store';

const { LOAD_ENDPOINT, UPDATE_ENDPOINT, RESET_ENDPOINT } = C;
const { SET_SCREEN, SET_TOKEN, SELECT_PHOTO } = C;


const app = handleActions({
    [SET_SCREEN]: (state={}, action) => {
        return (state.screen !== action.id) ?
            {
                ...state,
                screen: action.id,
            } : state;
    },
    [SET_TOKEN]: (state={}, action) => ({
        ...state,
        token: action.token,
    }),
    [SELECT_PHOTO]: (state={}, action) => ({
        ...state,
        selection: action.selection,
    }),
}, store.app);


const endpoint = {
    [UPDATE_ENDPOINT]: (endpoint={}, action) => {
        const data = {...action};
        delete data.id;
        delete data.type;
        return (endpoint.id === action.id) ?
            {...endpoint, ...data}
            : endpoint;
    },
    [LOAD_ENDPOINT]: (endpoint={}, action) => {
        return (endpoint.id === action.id) ?
            {
                ...endpoint,
                loading: true,
            } : endpoint;
    },
    [RESET_ENDPOINT]: (endpoint={}, action) => {
        return (endpoint.id === action.id) ?
            {
                ...endpoint,
                loaded: false,
                loading: false,
                loadStatus: 0,
                loadError: null,
                data: null,
                firstPage: null,
                vars: {},
                page: 0,
            } : endpoint;
    },
};

const endpoints = handleActions({
    [RESET_ENDPOINT]: (endpoints={}, action) => {
        return Object.keys(endpoints).reduce((acc, key) => (
            {...acc, [key]: endpoint[RESET_ENDPOINT](endpoints[key], action)}
        ), {});
    },
    [UPDATE_ENDPOINT]: (endpoints={}, action) => {
        return Object.keys(endpoints).reduce((acc, key) => {
            return (key === action.id)
                        ? {...acc, [key]: endpoint[UPDATE_ENDPOINT](endpoints[key], action)}
                        : {...acc, [key]: endpoints[key]};
        }, {});
    },
}, store.endpoints);

const reducers = combineReducers ({ app, endpoints });
export default reducers;