import { actions as CA } from '../constants';

const { SELECT_PHOTO, UPDATE_ENDPOINT, SET_SCREEN, RESET_ENDPOINT, SET_TOKEN } = CA;

const logger = (...args) => {
    console.log(args);
};

const loadEnpoint = (id, vars={}) => (dispatch, getState) => {
    let endpoint = getState().endpoints[id];

    if (endpoint && !endpoint.loading)  {
        dispatch (updateEndpoint(id, {
            loading: true,
        }));

        const server = getState().app.server.url;
        vars = {...vars, client_id: getState().app.token};
        let { url, method='get' } = endpoint;
        url = server+url;

        let params = { method };
        if (method.toLowerCase() === 'get') {
            const varsStr = Object.keys(vars).map((key) => `${key}=${vars[key]}`).join('&');
            url = (url.indexOf('?') === -1) ? url+'?'+varsStr : url+varsStr;
        } else if (method.toLowerCase() === 'post') {
            params = {...params, body: JSON.stringify(vars)};
        }

        logger('load enpoint:', url, method, vars, ';', ', next page-', ';', params);
        return fetch(url, params)
            .then(response => {
                response.json()
                    .then(data => {
                        let endpoint = getState().endpoints[id];
                        endpoint.loading && dispatch (updateEndpoint(id, {
                            loading: false,
                            loaded: (response.status === 200),
                            status: response.status,
                            data: {...data},
                        }));
                        logger('load success:', data);
                    });
            })
            .catch(error => {
                let endpoint = getState().endpoints[id];
                endpoint.loading && dispatch (updateEndpoint(id, {
                    loading: false,
                    status: (error.response) ? error.response.status : 0,
                    error: error.message,
                    data: null,
                }));
                logger('load error:', error.message);
            });
    }
};
const updateEndpoint = (id, payload={}) => {
    return {
        type: UPDATE_ENDPOINT,
        id,
        ...payload,
    }
};
const resetEndpoint = (id) => {
    return {
        type: RESET_ENDPOINT,
        id,
    }
};

const setToken = (token) => {
    return {
        type: SET_TOKEN,
        token,
    }
};
const setScreen = (id) => {
    return {
        type: SET_SCREEN,
        id,
    }
};
const selectPhoto = (selection) => {
    return {
        type: SELECT_PHOTO,
        selection,
    }
};


export { loadEnpoint, updateEndpoint, resetEndpoint, setScreen, setToken, selectPhoto };

















