const { endpoints } = require('../constants');
const { PHOTOS_LIST } = endpoints;

const store = {
    app: {
        screen: null,
        token: '',
        server: {
            name: 'Unsplash',
            url: 'https://api.unsplash.com',
        },
        selection: null,
    },
    endpoints: {
        [PHOTOS_LIST]: {
            id: PHOTOS_LIST,
            url: '/photos/',
            method: 'get',
            loading: false,
            loaded: false,
            status: 0,
            error: null,
            data: [],
        },
    }
};

export default store;