const actions = {
    SET_SCREEN: 'SET_SCREEN',
    SELECT_PHOTO: 'SELECT_PHOTO',
    SET_TOKEN: 'SET_TOKEN',
    LOAD_ENDPOINT: 'LOAD_LIST',
    UPDATE_ENDPOINT: 'UPDATE_ENDPOINT',
    RESET_ENDPOINT: 'RESET_ENDPOINT',
};

const endpoints = {
    PHOTOS_LIST: 'PHOTOS_LIST',
};

const screens = {
    LIST_SCREEN: 'LIST_SCREEN',
    ITEM_SCREEN: 'ITEM_SCREEN',
};

export {actions, endpoints, screens}