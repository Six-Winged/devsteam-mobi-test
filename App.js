/**
 Devsteam test task
 @author Alex T <alex.t.seraphim@gmail.com>
 */
import React, {Component} from 'react';
import Provider from "react-redux/es/components/Provider";
import {createStore, applyMiddleware} from "redux";
import thunk from "redux-thunk";
import reducers from "./src/modules/reducers";
import {loadEnpoint} from "./src/modules/actions";
import {endpoints as C} from "./src/constants";
import Devsteam from "./src/components/Devsteam/Devsteam";

const store = createStore(reducers, applyMiddleware(thunk));

type Props = {};
export default class App extends Component<Props> {
    render() {
        return (
            <Provider store={store}>
                <Devsteam />
            </Provider>
        );
    }
}
